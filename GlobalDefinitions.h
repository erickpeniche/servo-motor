#ifndef GLOBALDEFINITIONS_H
#define	GLOBALDEFINITIONS_H

#define true  1
#define false 0

#define _ON  1
#define _OFF 0

#define _INPUT 1
#define _OUTPUT 0

#define CCP1 PORTCbits.RC2
#define PWMS TRISCbits.TRISC2

#define LEDL PORTBbits.RB0
#define LEDR PORTBbits.RB1

#endif	/* GLOBALDEFINITIONS_H */