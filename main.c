#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "Config.h"
#include "lcd.h"

/**
* PIC Configurations
*/
#pragma config FOSC = XT
#pragma config WDTE = OFF
#pragma config PWRTE = OFF
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = OFF
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF

#pragma config BOR4V = BOR40V
#pragma config WRT = OFF

void interrupt isr(void);
void turn_motor(int degrees);
void set_timer2(int prsc);
void stop_motor(void);
void start_lcd(void);
void delay(float milliseconds);
void next_degree();

float pwmfreq = 250.0;
float pwmperiod = 0.0;
float oscfreq = 4000000.0;
float oscperiod = 0.0;
int pr2value = 0;
int maxdutycycle = 0;
int dutycycle = 0;
int started = 0;
int degreescount = 45;
int direction = 1;
int isButtonPressed = 0;

unsigned char buffer1[20];
unsigned char buffer2[20];

int main(int argc, char** argv) {
	config_board();
	start_lcd();
	turn_motor(degreescount);
	while(1) {
		sprintf(buffer1, "D: %d ", degreescount);									//convertimos el valor en ASCII
		Lcd_Out2(1, 1, buffer1);
		sprintf(buffer1, "Direction: %d ", direction);									//convertimos el valor en ASCII
		Lcd_Out2(2, 1, buffer1);
		if(PORTDbits.RD0 == 1) {
			if (isButtonPressed == 0)
			{
				turn_motor(degreescount);
				next_degree();
				LEDL = _ON;
			}
			isButtonPressed = 1;
		}
		else {
			isButtonPressed = 0;
			PORTB = _OFF;			// Turn off leds
			//stop_motor();
		}
	}

	return (EXIT_SUCCESS);
}

void next_degree() {
	degreescount += 45 * direction;
	if (degreescount >= 225) {
		direction = -1;
	} else if (degreescount <= 45) {
		direction = 1;
	}
}

void interrupt isr(void) {
	if (PIR1bits.TMR2IF == 1 && started == 0) {				// Se activa el evento especial
		started = 1;
		PWMS = _OUTPUT;						// CCP1 => OUTPUT
		CCP1 = _ON;

		__delay_ms(2000);
		stop_motor();
	}
	PIR1bits.TMR2IF = 0;
}

void stop_motor(void) {
	PWMS = _INPUT;						// CCP1 => OUTPUT

	PIE1bits.TMR2IE = 0;														// Enables the TMR2 interrupt
	PIR1bits.TMR2IF = 0;														// Clear TMR2 Flag
	CCP1 = _OFF;
	started = 0;
}

void turn_motor(int degrees) {
	dutycycle = (degrees*50)/180;
	oscperiod = (float)(1.0/oscfreq);
	pwmperiod = (float)(1.0/pwmfreq);

	for (int i = 1; i <= 16; i = i*4)
	{
		pr2value = (int)(pwmperiod/(i*oscperiod*4.0)-1.0);
		if(pr2value > 255 || pr2value < 1) {
			continue;
		}
		else {
			maxdutycycle = (int)(pwmperiod/(oscperiod*i));
			dutycycle = (int)((dutycycle/100.0)*((float)maxdutycycle));

			PIE1bits.TMR2IE = 1;														// Enables the TMR2 interrupt
			PIR1bits.TMR2IF = 0;														// Clear TMR2 Flag

			// Configure CCP1
			PR2 = (int)pr2value;
			CCP1CON = ((dutycycle & 3) << 4) + 12;								// Motor runs forward

			CCPR1L = dutycycle >> 2;

			// Configure TMR2
			set_timer2(i);
			break;
		}
	}
}

void set_timer2(int prsc) {
	if(prsc == 1) {
		T2CON = 0b00000100;
	}
	else if(prsc == 4) {
		T2CON = 0b00000101;
	}
	else if(prsc == 16) {
		T2CON = 0b00000111;
	}
}

void start_lcd(void) {
	Lcd_Init();
	Lcd_Cmd(LCD_CLEAR);
	Lcd_Cmd(LCD_CURSOR_OFF);
	__delay_ms(100);
}

void delay(float milliseconds) {
	int count = (int)milliseconds;
	for (int i = 0; i < count; i++) {
		__delay_ms(1);
	}
}