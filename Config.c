#include "Config.h"

void config_board(){
	/*--------76543210*/
	TRISA = 0b10000000;															// RA7/CLKIN => INPUT (CRYSTAL OSC)
	TRISB = 0b11100000;															// RB7/ICSPDAT => INPUT (PICKIT), RB6/ICSPCLK => INPUT (PICKIT), RB5/AN13 => INPUT (POTENTIOMETER)
	TRISC = 0b00000100;															// CCP1 => INPUT
	TRISD = 0b11100011;															// RD0 & RD1 => INPUT (Push buttons for controlling the motor)
	TRISE = 0b00000000;

	PORTA = PORTB = PORTC = PORTD = PORTE = 0;									// All ports are cleared

	/* ANALOG ports */
	/*---------76543210*/
	ANSEL  = 0b00000000;														// ANS<7:0> disabled
	ANSELH = 0b00100000;														// ANS13 enabled (POTENTIOMETER)

	/*
	 * ADCON0 bits
	 *  _________________________________________________________________
	 * |    7       6       5       4       3       2       1       0    |
	 * |-----------------------------------------------------------------|
	 * |    ADCS<1:0>   |            CHS<3:0>           |GO/DONE|  ADON  |
	 * |   00 = FOSC/2  |   0000 = AN0      0001 = AN1  |0=idle |1=ADC ON|
	 * |   01 = FOSC/8  |   0010 = AN2      0011 = AN3  |1=wrkng|0=ADC OF|
	 * |   10 = FOSC/32 |   0100 = AN4      0101 = AN5  |       |        |
	 * |   11 = FRC     |   0110 = AN6      0111 = AN7  |       |        |
	 * |                |   1000 = AN8      1001 = AN9  |       |        |
	 * |                |   1010 = AN10     1011 = AN11 |       |        |
	 * |                |   1100 = AN12     1101 = AN13 |       |        |
	 * |                |   1110 = CVREF    1111 = FR   |       |        |
	 * |________________|_______________________________|_______|________|
	 *
	 */
	ADCON0 = 0b01110101;														// See above chart for configuration
	ADCON1bits.ADFM = 1;														// ADFM: A/D Conversion Result Format = Right justified
	ADCON1bits.VCFG0 = 0;														// VCFG0: Voltage Reference bit = VDD
	ADCON1bits.VCFG1 = 0;														// VCFG1: Voltage Reference bit = VSS

	INTCONbits.GIE = 1;															// Enables Global Interrupts
	INTCONbits.PEIE = 1;														// Enables Peripherial Interrupts
}